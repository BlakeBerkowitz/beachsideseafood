﻿using BeachsideSeafood.UI.Models;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;

namespace IdentitySample.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Contact(ContactViewModel contact)
        {
            //Create the body for the email
            //write the letter
            string body = string.Format("Name: {0}<br/>Email: {1}<br/>" +
                "Subject: {2}<br/>{3}",
                contact.Name,
                contact.Email,
                contact.Subject,
                contact.Message);


            //Create and configure the mail message
            //in the envelope
            //must have created a no reply in hosting account
            MailMessage msg = new MailMessage("no-reply@blakeberkowitz.com", "blake@kc-berkowitz.com",
                contact.Subject, body);


            //Configure the mail message object 
            //mailman
            //to convert that all to HTML
            msg.IsBodyHtml = true;
            /*msg.CC.Add("");*/ //to send to someone else
            msg.Priority = MailPriority.High;

            //Create and configure the SMTP client 
            //Post office
            SmtpClient client = new SmtpClient("mail.philmadethat.com");
            client.Credentials = new NetworkCredential("no-reply@philmadethat.com", "P@ssw0rd");

            using (client)
            {
                try
                {
                    client.Send(msg);
                    ViewBag.Confirm = "Email sent successfully.";

                }
                catch
                {

                    ViewBag.ErrorMessage = "There was an error sending your email. Please try again later";
                    return View();
                }
            }

            //Send the user to the Contact Confirmation View
            //and pass the object with it
            //we got it

            return View(contact);
        }

    }
}
