﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BeachsideSeafood.Data;
using BeachsideSeafood.Data.Exceptions;
using Microsoft.AspNet.Identity;

namespace BeachsideSeafood.UI.Controllers
{
    public class ApplicationsController : Controller
    {
        //private BeachsideSeafoodEntities db = new BeachsideSeafoodEntities();
        unitofwork uow = new unitofwork();


        // GET: Applications
        public ActionResult Index()
        {
            //var applications = uow.Applications.Include(a => a.OpenPositionID1);
            if (User.IsInRole("Manager") || User.IsInRole("Admin"))
            {
                return View(uow.ApplicationRepository.Get("OpenPositionID1"));

            }
            else
            {
                var application = uow.ApplicationRepository.Get().Where(x => x.UserID == User.Identity.GetUserId());

                return View(application);
            }

            }
        

        // GET: Applications/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = uow.ApplicationRepository.Find(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            return View(application);
        }

        // GET: Applications/Create
        public ActionResult Create(int? id)
        {

            if (id.HasValue)
                ViewBag.OpenPositionID = id;
            //    ViewBag.OpenPositionID = id.Value;
                
            //ViewBag.OpenPositionID = new SelectList(uow.OpenPositionIDRepository.Get(), "OpenPositionID1", "OpenPositionID1",id);


            ViewBag.UserID = new SelectList(uow.UserRepository.Get(), "Id", "Id", User.Identity.GetUserId()); //TO DO fix this to transfer aspnet user id to user id for application
            return View();
        }



        // POST: Applications/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ApplicationID,OpenPositionID,UserID,ApplicationDate,ManagerNotes,IsDeclined,ResumeFilename")] Application application, HttpPostedFileBase resumeFilename)
        {
            if (ModelState.IsValid)
            {
                #region File Upload
                //default the value of the image name to our no phote name
                //noimage.jpg
                string resumeName = "";

                //see if the user uploaded an image
                if (resumeFilename != null)
                {
                    //get filename
                    resumeName = resumeFilename.FileName;

                    //use the filename to get the extension
                    string ext = resumeName.Substring(resumeName.LastIndexOf('.'));

                    //create a list of VALID extensions
                    //checking to make sure that malicious code is not uploaded
                    //to our application
                    string[] goodExts = new string[]
                        {".docx",".doc",".pdf",".rtf",".html",".odf",".txt" };

                    //if the extension is not contained in that list
                    if (!goodExts.Contains(ext))
                    {
                        ////rename it back to noImage.jpg
                        //imageName = "noimage.jpg";//default imagename

                        //lets throw the flag on this play - cause exception that explains problem
                        //log.Info(User.Identity.Name + " tried to upload a bad file of type" + ext);
                        throw new InvalidFileTypeException("invalid file type uploaded. only image filetypes are allowed.");
                    }//end if
                     //else
                    else
                    {
                        //rename the image GUID - Global Unique IDentifier
                        resumeName = Guid.NewGuid() + ext;

                        //save the file to the webserver
                        resumeFilename.SaveAs(Server.
                            MapPath("~/Content/Resumes/" + resumeName));

                        ////using our service
                        string savePath = Server.MapPath("~/Content/Resumes/");
                        ////convert the file from httppostedfilebbase to image
                        //Image convertedImage = Image.FromStream(bookImage.InputStream);
                        ////set the max size for the full size images in px
                        //int maxImageSize = 500;

                        ////set the max size for thumbs
                        //int maxThumbSize = 100;

                        ////call the image service to resize and sve the image
                        //ImageServices.ResizeImage(savePath, imageName, convertedImage, maxImageSize, maxThumbSize);

                    }//end else
                }
                //no matter what save the filename to the database
                application.ResumeFilename = resumeName;

                #endregion
                //application.UserID = application.AspNetUser.UserName;
                application.ApplicationDate = DateTime.Now;
                application.UserID = User.Identity.GetUserId();
                uow.ApplicationRepository.Add(application);
                uow.Save();
                return RedirectToAction("Index");
            }

            ViewBag.OpenPositionID = new SelectList(uow.OpenPositionIDRepository.Get(), "OpenPositionID1", "OpenPositionID1", application.OpenPositionID);
            return View(application);
        }

        // GET: Applications/Edit/5
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Application application = uow.ApplicationRepository.Find(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            ViewBag.OpenPositionID = new SelectList(uow.OpenPositionIDRepository.Get(), "OpenPositionID1", "OpenPositionID1", application.OpenPositionID);
            return View(application);
        }

        // POST: Applications/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ApplicationID,OpenPositionID,UserID,ApplicationDate,ManagerNotes,IsDeclined,ResumeFilename")] Application application)
        {
            if (ModelState.IsValid)
            {
                uow.ApplicationRepository.Update(application);
                uow.Save();
                return RedirectToAction("Index");
            }
            ViewBag.OpenPositionID = new SelectList(uow.OpenPositionIDRepository.Get(), "OpenPositionID1", "OpenPositionID1", application.OpenPositionID);
            return View(application);
        }

	
        // GET: Applications/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Application application = uow.ApplicationRepository.Find(id);
            if (application == null)
            {
                return HttpNotFound();
            }
            return View(application);
        }

        // POST: Applications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Application application = uow.ApplicationRepository.Find(id);
            uow.ApplicationRepository.Remove(application);
            uow.Save();
            return RedirectToAction("Index");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
