﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BeachsideSeafood.Data;

namespace BeachsideSeafood.UI.Controllers
{
    public class OpenPositionIDsController : Controller
    {
        //private BeachsideSeafoodEntities db = new BeachsideSeafoodEntities();
        unitofwork uow = new unitofwork();

        // GET: OpenPositionIDs
        public ActionResult Index()
        {
            var openPositionIDs = uow.OpenPositionIDRepository.Get();
            return View(openPositionIDs.ToList());
        }

        // GET: OpenPositionIDs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OpenPositionID openPositionID = uow.OpenPositionIDRepository.Find(id);
            if (openPositionID == null)
            {
                return HttpNotFound();
            }
            return View(openPositionID);
        }

        // GET: OpenPositionIDs/Create
        public ActionResult Create()
        {
            ViewBag.LocationID = new SelectList(uow.LocationRepository.Get(), "LocationID", "StoreNumber");
            ViewBag.PositionID = new SelectList(uow.PositionRepository.Get(), "PositionID", "Title");
            return View();
        }

        // POST: OpenPositionIDs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OpenPositionID1,PositionID,LocationID")] OpenPositionID openPositionID)
        {
            if (ModelState.IsValid)
            {
                uow.OpenPositionIDRepository.Add(openPositionID);
                uow.Save();
                return RedirectToAction("Index");
            }

            ViewBag.LocationID = new SelectList(uow.LocationRepository.Get(), "LocationID", "StoreNumber", openPositionID.LocationID);
            ViewBag.PositionID = new SelectList(uow.PositionRepository.Get(), "PositionID", "Title", openPositionID.PositionID);
            return View(openPositionID);
        }

        // GET: OpenPositionIDs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OpenPositionID openPositionID = uow.OpenPositionIDRepository.Find(id);
            if (openPositionID == null)
            {
                return HttpNotFound();
            }
            ViewBag.LocationID = new SelectList(uow.LocationRepository.Get(), "LocationID", "StoreNumber", openPositionID.LocationID);
            ViewBag.PositionID = new SelectList(uow.PositionRepository.Get(), "PositionID", "Title", openPositionID.PositionID);
            return View(openPositionID);
        }

        // POST: OpenPositionIDs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OpenPositionID1,PositionID,LocationID")] OpenPositionID openPositionID)
        {
            if (ModelState.IsValid)
            {
                uow.OpenPositionIDRepository.Update(openPositionID);
                uow.Save();
                return RedirectToAction("Index");
            }
            ViewBag.LocationID = new SelectList(uow.LocationRepository.Get(), "LocationID", "StoreNumber", openPositionID.LocationID);
            ViewBag.PositionID = new SelectList(uow.PositionRepository.Get(), "PositionID", "Title", openPositionID.PositionID);
            return View(openPositionID);
        }

        // GET: OpenPositionIDs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OpenPositionID openPositionID = uow.OpenPositionIDRepository.Find(id);
            if (openPositionID == null)
            {
                return HttpNotFound();
            }
            return View(openPositionID);
        }

        // POST: OpenPositionIDs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OpenPositionID openPositionID = uow.OpenPositionIDRepository.Find(id);
            uow.OpenPositionIDRepository.Remove(openPositionID);
            uow.Save();
            return RedirectToAction("Index");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
