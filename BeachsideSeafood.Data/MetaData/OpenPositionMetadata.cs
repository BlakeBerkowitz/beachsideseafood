﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeachsideSeafood.Data.MetaData
{
    public class OpenPositionMetadata
    {

        public int OpenPositionID1 { get; set; }
        public int PositionID { get; set; }
        public int LocationID { get; set; }

    }
}
