﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeachsideSeafood.Data
{
    public class PositionMetadata
    {


        public int PositionID { get; set; }
        public string Title { get; set; }
        public string JobDescription { get; set; }

    }
}
