﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeachsideSeafood.Data
{
    [MetadataType(typeof(ApplicationMetadata))]

    public partial class Application { }

    public class ApplicationMetadata
    {

        public int ApplicationID { get; set; }
        public int OpenPositionID { get; set; }
        public string UserID { get; set; }

        [Required]
        [DisplayFormat(DataFormatString ="{0:d}")]
        public System.DateTime ApplicationDate { get; set; }
        public string ManagerNotes { get; set; }
        public Nullable<bool> IsDeclined { get; set; }
        [Required]
        public string ResumeFilename { get; set; }


    }
}
