﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BeachsideSeafood.Data.Repositories;

namespace BeachsideSeafood.Data
{

    public interface IUnitofWork : IDisposable
    {
        void Save();
    }

    public class unitofwork : IUnitofWork
    {
        //fields
        internal BeachsideSeafoodEntities ctx = new BeachsideSeafoodEntities();
        private PositionRepository _positionRepository;
        private LocationRepository _locationRepository;
        private OpenPositionIDRepository _openPositionIDRepository;
        private ApplicationRepository _applicationRepository;
        private UserRepository _userRepository;

        //props

        public PositionRepository PositionRepository
        {
            get
            {
                if(this._positionRepository == null)
                {
                    this._positionRepository = new PositionRepository(ctx);
                }
                return _positionRepository;
            }

        }

        public OpenPositionIDRepository OpenPositionIDRepository
        {
            get
            {
                if (this._openPositionIDRepository == null)
                {
                    this._openPositionIDRepository = new OpenPositionIDRepository(ctx);
                }
                return _openPositionIDRepository;
            }

        }

        public LocationRepository LocationRepository
        {
            get
            {
                if (this._locationRepository == null)
                {
                    this._locationRepository = new LocationRepository(ctx);
                }
                return _locationRepository;
            }

        }

        public ApplicationRepository ApplicationRepository
        {
            get
            {
                if (this._applicationRepository == null)
                {
                    this._applicationRepository = new ApplicationRepository(ctx);
                }
                return _applicationRepository;
            }

        }

        public UserRepository UserRepository
        {
            get
            {
                if (this._userRepository == null)
                {
                    this._userRepository = new UserRepository(ctx);
                }
                return _userRepository;
            }

        }






        //methods

        public void Save()
        {
            ctx.SaveChanges();

        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



    }
}
