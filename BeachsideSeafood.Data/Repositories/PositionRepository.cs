﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeachsideSeafood.Data.Repositories
{
    public interface IPositionRepository<TEntity> : IDisposable where TEntity : class { }

    public class PositionRepository : GenericRepository<Position>
    {
        public PositionRepository(DbContext db) : base(db) { }
    }
}
