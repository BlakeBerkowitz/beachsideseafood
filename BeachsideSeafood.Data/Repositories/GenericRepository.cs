﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeachsideSeafood.Data.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        //private cStoreEntities db = new cStoreEntities();

        //implemenatation of unit of work (root of the domain layer)
        internal DbContext _db;
        public GenericRepository(DbContext db)
        {
            this._db = db;
        }

        //crud
        //virtual keyword allows for overriding

        public virtual List<TEntity> Get(string includeProperties = "")
        {

            #region Linq inferred typing (IQueryable)


            ////results of a linq statement are of type iqueryable by default. if you call the .single()
            ////the type changes to be a single instance of that type
            //var x = from y in db.Authors select y;

            //var a = (from b in db.Authors where b.FirstName == "blake" select b).Single();
            #endregion
            #region why is this code here instead of the Get() original code?


            /*
            this generic repository would work fine for simple tables but those such as the Title table which use LiNQ
            to include other table data are not going to recieve all of the information that is required. (the .Include() or joins)

            */
            #endregion
            /*
            holding variable for the data to be returned because of inferred typing (see note above) we want this
            result queryable by Linq, hence the datatype of IQueryable

            */
            IQueryable<TEntity> query = _db.Set<TEntity>();
            //this loop cares for the string value that was passed in to the method when it was called. if no values
            //were passed then the value evaluated is String.empty (as opposed to null or 0)
            //var [var] in [value passed as a param to the method]
            foreach (var prop in includeProperties.
                //[method identifier](ne char[]{'[someChar]'})
                Split(new char[] { ',' },
                //[enum for StringSplit].[Value]
                StringSplitOptions.RemoveEmptyEntries))

            {
                //add the property to be included in the query to return the value
                query = query.Include(prop);
            }
            //send the query with an includes back to the requester
            return query.ToList();

            //original return form basic generic get
            //return db.Set<TEntity>().ToList();

        }

        public TEntity Find(object id)
        {
            return _db.Set<TEntity>().Find(id);

        }

        public void Add(TEntity entity)
        {
            _db.Set<TEntity>().Add(entity);
            _db.SaveChanges();

        }
        public void Update(TEntity entity)
        {
            _db.Entry(entity).State = EntityState.Modified;
            _db.SaveChanges();

        }

        public void Remove(TEntity entity)
        {
            _db.Set<TEntity>().Remove(entity);
            _db.SaveChanges();
        }
        public void Remove(object id)
        {
            var entity = _db.Set<TEntity>().Find(id);
            Remove(entity);
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            /*
            all the notes go here*/
            GC.SuppressFinalize(this);


        }

        public int CountRecords()
        {
            //throw new NotImplementedException();
            //default implementation creates method with the correct signiature but has no real functionality
            //essentially gone from build error to run time error

            return Get().Count();
            //count as a method expects  a list to be returned, get returns an iqueryable which belongs to entity the count used above also belogs to entity


        }
        //un comment if add the search nuget       :)
        ////search info - after adding the requirement to the interface

        //public IQueryable<TEntity> Search(Expression<Func<TEntity, bool>> predicate)
        //{
        //    return _db.Set<TEntity>().AsExpandable().Where(predicate);
        //}
    }
}
