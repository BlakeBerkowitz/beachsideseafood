﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BeachsideSeafood.Data.Repositories
{
    public interface IGenericRepository<TEntity> : IDisposable where TEntity : class
    {
        //fields
        //props
        //ctors
        //methods
        List<TEntity> Get(string includeProperties = "");
        TEntity Find(object id);
        void Add(TEntity entity);
        void Update(TEntity entity);
        void Remove(object id);
        void Remove(TEntity entity);

        //i want a method that returns an int value that counts the number of records in the able
        int CountRecords();

        //uncomment if adding the search nuget
        //IQueryable<TEntity> Search(Expression<Func<TEntity, bool>> predicate);

    }
}
