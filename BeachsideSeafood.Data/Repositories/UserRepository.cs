﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeachsideSeafood.Data.Repositories
{
    public interface IUserRepository<TEntity> : IDisposable where TEntity : class { }

    public class UserRepository : GenericRepository<AspNetUser>
    {
        public UserRepository(DbContext db) : base(db) { }

    }
}
