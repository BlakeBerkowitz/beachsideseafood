﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeachsideSeafood.Data.Repositories
{

    public interface IApplicationRepository<TEntity> : IDisposable where TEntity : class { }

    
    public class ApplicationRepository : GenericRepository<Application>
    {
        public ApplicationRepository(DbContext db) : base(db) { }
    }
}
