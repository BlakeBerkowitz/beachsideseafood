﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeachsideSeafood.Data.Repositories
{
    public interface IOpenPositionRepository<TEntity> : IDisposable where TEntity : class { }
    public class OpenPositionIDRepository : GenericRepository<OpenPositionID>
    {
        public OpenPositionIDRepository(DbContext db) : base(db) { }

    }
}
