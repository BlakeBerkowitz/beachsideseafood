﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeachsideSeafood.Data.Repositories
{
    public interface ILocationRepository<TEntity> : IDisposable where TEntity : class { }

    public class LocationRepository : GenericRepository<Location>
    {
        public LocationRepository(DbContext db) : base(db) { }

      
    }
}
